--farm and dirt

farm = content:new("farm")

function farm:init()
	self.i = globals.materials.farm
	self.flags.farmable=true
	self.flags.diggable = false
	self.quality = 0.10 --farm fertility how often it produces food
	self.fertile = true --whether it is still earth
end

function farm:update(dt)
	if math.random() <= self.quality then
		globals.food = globals.food + 1
	end
	if math.random() <= 0.80 then
		self.quality = self.quality - 0.01
	end

	--if math.random() <= 0.1 then
		self:tend()
	--end

	if self.quality <= 0.10 and self.fertile then
		self.sprite = love.graphics.newImage( "block/dirt.png" )
		self.fertile = false
		self.flags.farmable = false
		self.flags.diggable = true
	end

	if self.quality >= 0.10 and not self.fertile then
		self.sprite = love.graphics.newImage( "block/farm.png" )
		self.fertile = true
		self.flags.farmable = true
		self.flags.diggable = false
	end
end

function farm:tend()
	if math.random() <= 0.80 then
		self.quality = self.quality + 0.01
	end
end
