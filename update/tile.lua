require "../cell"
require "../globals"

local channel = love.thread.getChannel("tile")

if channel:getCount() > 0 then
	local dt = channel:pop()
	local i = channel:pop()

	globals.tiles[i]:update(dt)
	print("Hello from tile my dt is "..dt)
end
