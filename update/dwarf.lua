require "../globals"

local dwarf_channel = love.thread.getChannel("dwarf")
local channel = dwarf_channel:demand()

local dwarf = channel:demand()

math.randomseed(dwarf.dt)

local dtime = 0

while dwarf.alive do
	local _dwarf = channel:peek()
	if _dwarf ~= nil then
		if _dwarf ~= dwarf then
			dwarf = channel:pop()
		end
	end
	if dtime >= dwarf.speed then
		dtime = dtime - 1
		if math.random(100)%2 == 0 then
			if math.random(100)%2 == 0 then
				if dwarf.x < globals.map_w-2 then
					if dwarf.right == 0 then
						dwarf.x = dwarf.x + 1
					end
				end
			else
				if dwarf.y < globals.map_h then
					if dwarf.down == 0 then
						dwarf.y = dwarf.y + 1
					end
				end
			end
		else
			if math.random(100)%2 == 0 then
				if dwarf.x > 1 then
					if dwarf.left == 0 then
						dwarf.x = dwarf.x - 1
					end
				end
			else
				if dwarf.y >40 then
					if dwarf.up == 0 then
						dwarf.y = dwarf.y - 1
					end
				end
			end
		end
	else
		dtime = dtime + dwarf.dt
	end
	channel:push(dwarf)
end
