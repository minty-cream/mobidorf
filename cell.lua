-- a cell contains something

cell = class()

function cell:init(a,x,y)
	self.x = x
	self.y = y
	self._width = globals.cell_width
	self._height = globals.cell_height
	self.content = a
	self.highlighted = false
	self.clicked = false
end

function cell:update(dt)
	self:highlight()
	if self.highlighted then
		if love.mouse.isDown("l") then
			self.content = content:new(globals.selected,self.x,self.y)
			globals.map[self.y/globals.cell_height][self.x/globals.cell_width]=self.content.i
			output = ""

			for k,v in pairs(globals.map) do
				for j,u in pairs(v) do
					output = output..u
				end
				output = output.."\n"
			end
			--print(output)
		end
	end
	self.content:update(dt)
end

function cell:draw(dt)
	if self.highlighted then
		love.graphics.setColor(255,0,255,200)
		self._width = globals.cell_width
		self._height = globals.cell_height
		if love.mouse.isDown("r") then
			love.graphics.reset()
			love.graphics.setColor(255,100,100)
			self._width = globals.cell_width*3
			self._height = globals.cell_height*4
		end
	end
	if self.highlighted then
		love.graphics.rectangle("fill",self.x,self.y,self._width,self._height)
		love.graphics.reset()
	end
end

function cell:highlight()
	local _x = love.mouse.getX()
	local _y = love.mouse.getY()
	self.highlighted = self.x  < _x and self.x + globals.cell_width> _x and self.y  < _y and self.y + globals.cell_height > _y
end
