tile = class:new()

function tile:init(x,y)
	self.x = x
	self.y = y
	self.map = {
	{cell:new(0),cell:new(0),cell:new(0)},
	{cell:new(0),cell:new(0),cell:new(0)},
	{cell:new(0),cell:new(0),cell:new(1)}
	}
--	{cell:new(math.random(0,1)),cell:new(math.random(0,1)),cell:new(math.random(0,1))},
--	{cell:new(math.random(0,1)),cell:new(math.random(0,1)),cell:new(math.random(0,1))},
--	{cell:new(math.random(0,1)),cell:new(math.random(0,1)),cell:new(math.random(0,1))}
end

function tile:draw(map_x, map_y)
	for k,v in pairs(self.map) do
		for j,u in pairs(v) do
			love.graphics.draw(u.content, (map_x+j*self.x)*globals.cell_width, (map_y+k*self.y)*globals.cell_height)
		end
	end
end
