-- the dwarf entity

content = class()

function content:init(a)
	self.sprite = love.graphics.newImage( "block/"..a..".png" )
	self.i = globals.materials[a]
	self.flags = {
		diggable = true,
		minable = false,
		farmable = false
	}
end

function content:update(dt)
end
