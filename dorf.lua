dwarf = class()

function dwarf:init(h,w)
	self._dwarf = {}
	--self._dwarf.x = math.random(0,20)
	--self._dwarf.y = 1
	self._dwarf.x = math.random(0,w)
	self._dwarf.y = h
	self._dwarf.alive = true
	self._dwarf.strength = math.random(3,18)
	self._dwarf.dex = math.random(3,18)
	self._dwarf.mind = math.random(3,18)
	self._dwarf.i = math.random(0,2)
	self._dwarf.up = 1
	self._dwarf.down = 1
	self._dwarf.left = 1
	self._dwarf.right = 1
	if self._dwarf.strength > self._dwarf.dex and self._dwarf.strength > self._dwarf.mind then
		self._dwarf.purpose = "mine"
	elseif self._dwarf.dex > self._dwarf.mind and self._dwarf.dex > self._dwarf.strength then
		self._dwarf.purpose = "craft"
	elseif self._dwarf.mind > self._dwarf.strength and self._dwarf.mind > self._dwarf.dex then
		self._dwarf.purpose = "farm"
	else
		if self._dwarf.mind < 9 then
			self._dwarf.mind = self._dwarf.mind + 10
			self._dwarf.dex = self._dwarf.dex + 10
			self._dwarf.strength = self._dwarf.strength + 10
		end
		self._dwarf.purpose = "wander"
	end
	self._dwarf.speed = 19 - self._dwarf.dex
	self._dwarf.dt=0
	self.channel = love.thread.newChannel()
	self.thread = love.thread.newThread("update/dwarf.lua")
	self.channel:push(self._dwarf)
	love.thread.getChannel("dwarf"):push(self.channel)
	self.thread:start()
end

function dwarf:update(dt,up,down,left,right)
	self._dwarf.dt=dt
	self._dwarf.up=up
	self._dwarf.down=down
	self._dwarf.left=left
	self._dwarf.right=right
	self.channel:push(self._dwarf)

	if self.channel:peek() ~= nil then
		self._dwarf=self.channel:pop()
	end
	if not self._dwarf.alive then
		globals.dwarf_count = globals.dwarf_count - 1
	end
end
