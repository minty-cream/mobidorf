globals = {
	cell_width=20,
	cell_height=20,
	tiles = {},
	tileset = {},
	dwarves = {},
	dwarf_count = 0,
	food=10,
	materials = {
		stone = 3,
		farm = 2,
		dirt = 1,
		air = 0
	},
	selected = "air",
	dwarfChannel = love.thread.getChannel("dwarf"),
	map_display_w = 4*3,
	map_display_h = 7*3,
	map_w = 4*3,
	map_h = 7*3*4
}
