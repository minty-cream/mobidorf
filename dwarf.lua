-- the dwarf entity

dwarf = class()

function dwarf:init(i)
	self.map_x = math.random(0,20)
	self.map_y = 1
	self.x = self.map_x*20
	self.y = self.map_y*20
	self.alive = true
	self.strength = math.random(3,18)
	self.dex = math.random(3,18)
	self.mind = math.random(3,18)
	self.i = math.random(0,2)
	--self.sprite = love.graphics.newImage( "dwarf/"..(self.i-3)..".png" )
	self.skills = {
		mining = math.random(0,2),
		crafting = math.random(0,2),
		farming = math.random(0,2),
		fighting = math.random(0,2),
		training = math.random(0,2)
	}
end

function dwarf:update(dt)
	local channel = love.thread.getChannel("dwarf")
	channel.push({
		globals.map[self.map_y][self.map_x+1],
		globals.map[self.map_y+1][self.map_x],
		globals.map[self.map_y][self.map_x-1],
		globals.map[self.map_y-1][self.map_x]
	})
	channel.push({
		"alive"=>self.alive,
		"x"=>self.x,
		"y"=>self.y
	})
	self:eat()
end

function dwarf:eat()
	if math.random() <= 0.30 then
		if globals.food > 1 then
			globals.food = globals.food - math.random(1,5)
		else
			self.alive = false
			--self.sprite = love.graphics.newImage( "block/farm.png" )
			self.i = -1
			globals.dwarf_count = globals.dwarf_count - 1
		end
	end
end
