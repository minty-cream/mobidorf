
function love.load()

	math.randomseed( os.time() )
	math.randomseed( math.random() )

	require "globals"
	require "class"
	require "content"
	require "content/farm"
	require "cell"
	require "tile"
	require "dorf"
	-- our tiles

	love.graphics.setNewFont(12)
	background = love.graphics.newImage("background.png")
	number_map={}


	tileset = love.graphics.newImage("block/tileset.png")
	tileset:setFilter("nearest", "linear")
	tilesize = 21
	globals.tileset[0] = love.graphics.newQuad(0*tilesize,0*tilesize,tilesize,tilesize,tileset:getWidth(),tileset:getHeight())
	globals.tileset[1] = love.graphics.newQuad(1*tilesize,0*tilesize,tilesize,tilesize,tileset:getWidth(),tileset:getHeight())
	globals.tileset[2] = love.graphics.newQuad(1*tilesize,1*tilesize,tilesize,tilesize,tileset:getWidth(),tileset:getHeight())
	globals.tileset[3] = love.graphics.newQuad(0*tilesize,1*tilesize,tilesize,tilesize,tileset:getWidth(),tileset:getHeight())

	--dwarves
	globals.tileset[4] = love.graphics.newQuad(0*tilesize,2*tilesize,tilesize,tilesize,tileset:getWidth(),tileset:getHeight())
	globals.tileset[5] = love.graphics.newQuad(0*tilesize,3*tilesize,tilesize,tilesize,tileset:getWidth(),tileset:getHeight())
	globals.tileset[6] = love.graphics.newQuad(1*tilesize,2*tilesize,tilesize,tilesize,tileset:getWidth(),tileset:getHeight())

	tilesetBatch = love.graphics.newSpriteBatch(tileset, 21*21)

	-- map variables
	map_x = 0
	map_y = 3
	love.window.setMode(globals.map_display_w*globals.cell_width,globals.map_display_h*globals.cell_height)

	for y=0,globals.map_display_h-1 do
		number_map[y]={}
		for x=1,globals.map_w do
			number_map[y][x] = 0
		end
	end
	number_map[globals.map_display_h]={}
	for x=1,globals.map_w do
		number_map[globals.map_display_h][x] = math.random(0,2)
	end
	for y=globals.map_display_h+1,globals.map_h do
		number_map[y]={}
		table.insert(number_map[y],globals.materials.stone)
		for x=1,globals.map_w-2 do
			if math.random() > y/(globals.map_h-((globals.map_display_h+1)/globals.map_h)) then
				table.insert(number_map[y],globals.materials.air)
			else

				if math.random() < 0.5 then
					table.insert(number_map[y],globals.materials.dirt)
				else
					table.insert(number_map[y],globals.materials.stone)
				end
			end
		end
		table.insert(number_map[y],globals.materials.stone)
	end
	for y,a in pairs(number_map) do
		for x,z in pairs(a) do
			io.write(z)
		end
		io.write('\n')
	end
	--table.insert(globals.dwarves,_dwarf[0]:new())
	for x=0,0 do
		globals.dwarf_count = globals.dwarf_count + 1;
		table.insert(globals.dwarves,dwarf:new(globals.map_display_h-2,globals.map_display_w))
	end
end

function love.update(dt)
	if globals.dwarf_count <= 0 then
		print("You lose good day.")
		--love.event.quit()
	end
	tilesetBatch:clear()
	for y=1,globals.map_display_h do
		for x=1,globals.map_display_w do
			tilesetBatch:add(globals.tileset[number_map[y+map_y][x+map_x]],(x-1)*globals.cell_width,(y-1)*globals.cell_height)
		end
	end
	if globals.dwarf_count < 10 then
		globals.dwarf_count = globals.dwarf_count + 1;
		table.insert(globals.dwarves,dwarf:new(globals.map_display_h-2,globals.map_display_w))
	end
	for i,v in pairs(globals.dwarves) do
		--v:update(dt,number_map[v._dwarf.y-1][v._dwarf.x],number_map[v._dwarf.y+1][v._dwarf.x],number_map[v._dwarf.y][v._dwarf.x-1],number_map[v._dwarf.y][v._dwarf.x+1])
		v:update(dt,0,0,0,0)
		if v._dwarf.y >= map_y and v._dwarf.x >= map_x and v._dwarf.y < map_y+globals.map_display_h and v._dwarf.x < map_x+globals.map_display_w then
			tilesetBatch:add(globals.tileset[v._dwarf.i+4],(v._dwarf.x-map_x)*globals.cell_width,(v._dwarf.y-map_y)*globals.cell_height)
		end
	end
	--for i,v in pairs(globals.tiles) do
	--v:update(dt)
	--end
end

function love.keypressed(key, unicode)
	if key == 'up' then
		map_y = map_y-1
		if map_y < 0 then map_y = 0; end
	end
	if key == 'down' then
		map_y = map_y+1
		if map_y > globals.map_h-globals.map_display_h then map_y = globals.map_h-globals.map_display_h; end
	end

	if key == 'left' then
		map_x = math.max(map_x-1, 0)
	end
	if key == 'right' then
		map_x = math.min(map_x+1, globals.map_w-globals.map_display_w)
	end

	if key == '1' then
		globals.selected = "air"
	elseif key == '2' then
		globals.selected = "dirt"
	elseif key == '3' then
		globals.selected = "farm"
	elseif key == '4' then
		globals.selected = "stone"
	end


end

function love.draw()
	love.graphics.draw(background,0,0)
	love.graphics.draw(tilesetBatch)
	--for i,v in pairs(globals.tiles) do
	--love.graphics.draw(v.content.sprite, v.x-map_x*globals.cell_width, v.y-map_y*globals.cell_height)
	--end
	--for i,v in pairs(globals.dwarves) do
	--love.graphics.draw( v.sprite, v.x-map_x*globals.cell_width, v.y-map_y*globals.cell_height)
	--end
	--for i,v in pairs(globals.tiles) do
	--v:draw()
	--end
	love.graphics.print("Current FPS: "..tostring(love.timer.getFPS( )), 10, 10)
	love.graphics.print("Current Dwarves: "..globals.dwarf_count, 10, 10+love.graphics.getFont():getHeight()*2)
end
